#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/. 
#

###
### Build Stage
###

FROM alpine:latest as base
RUN apk update \
    && apk add --no-cache \
      autoconf \
      binutils \
      build-base \
      cmake \
      curl \
      file \
      gcc \
      g++ \
      git \
      libgcc \
      libtool \
      linux-headers \
      make \
      musl-dev \
      ninja \
      pkgconf \
      tar \
      unzip \
      wget \
      zip

FROM base as vcpkg

ENV VCPKG_ROOT /opt/vcpkg
ENV VCPKG ${VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake
ENV PATH=${VCPKG_ROOT}:$PATH
ENV VCPKG_FORCE_SYSTEM_BINARIES 1 

# install vcpkg
RUN git clone --depth=1 https://github.com/microsoft/vcpkg ${VCPKG_ROOT} \
  && bootstrap-vcpkg.sh -disableMetrics -useSystemBinaries

# Appends set(VCPKG_BUILD_TYPE release) to triplet file
#RUN sh -c "echo \"set(VCPKG_BUILD_TYPE release)\" >> ${VCPKG_ROOT}/triplets/x64-linux.cmake"

COPY x64-linux-musl.cmake ${VCPKG_ROOT}/triplets/

FROM vcpkg as restiniodev
LABEL description="Build container for dockerized-restinio"

# Install required libraries
RUN vcpkg install \
      asio \
      restinio \
      http-parser \
      fmt \
    && \
    rm -rf ${VCPKG_ROOT}/buildtrees/* && \
    rm -rf ${VCPKG_ROOT}/downloads/*

FROM restiniodev as build

# Prepare build environment
WORKDIR /app
COPY ./ ./
ENV CTEST_OUTPUT_ON_FAILURE=1

RUN cmake -S. -B_build -DCMAKE_TOOLCHAIN_FILE=$VCPKG -DVCPKG_TARGET_TRIPLET=x64-linux-musl -DCMAKE_BUILD_TYPE=Release \
    && cmake --build _build \
    && cd _build \
    && ctest

###
### Runtime stage
###

FROM alpine:latest as runtime
LABEL description="Runtime container for web app"

RUN apk update && apk add --no-cache libstdc++
COPY --from=build /app/_build/hello_restinio /usr/local/bin/

ENTRYPOINT [ "/usr/local/bin/hello_restinio" ]

EXPOSE 4050
