This repository contains a simple example that use [RESTinio](https://stiffstream.com/en/products/restinio.html) via the corresponding vcpkg package.

# How To Try

## Docker
The simplest way is to use Docker and Dockerfile from the repository. For example:
```bash
git clone https://gitlab.com/aarkhan/restinio-example
cd restinio-example
docker build -t restinio-example .
docker run -p 4050:4050 restinio-example &
curl http://localhost:4050/
```

## Manual Build
To perform manual build it is necessary to have vcpkg and CMake installed.
Environment variable VCPKG must contain path to $VCPKG_ROOT/scripts/buildsystems/vcpkg.cmake.
Then you can do the following steps:
```bash
# Install dependencies
vcpkg install restinio http-parser fmt openssl
# Clone the demo repository.
git clone https://gitlab.com/aarkhan/restinio-example
# Build the example.
cmake -S restinio-example -B build -D CMAKE_TOOLCHAIN_FILE=$VCPKG
cmake --build build
# Check the example.
./build/hello_restinio &
curl http://localhost:4050/
```
