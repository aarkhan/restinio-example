#include <restinio/all.hpp>
#include <fmt/format.h>

int main()
{
    char hname[128];
    gethostname(hname, sizeof(hname));
    std::string hostname(hname);
    int visits = 0;

    restinio::run(
        restinio::on_this_thread<>()
            .port(4050)
            .address("0.0.0.0")
            .request_handler([hostname, &visits](auto req) {
                std::string hello = fmt::format("Hello!\nYou reached {}\nVisits: {}", hostname, ++visits);
                return req->create_response().set_body(hello).done();
            }));

    return 0;
}
