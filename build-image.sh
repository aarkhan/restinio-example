#!/bin/sh
set -e

case $(uname -m) in
 aarch64)  TAG=arm64 ;;
 armv7l)   TAG=arm   ;;
 x86_64)   TAG=amd64 ;;
 *) echo Unsupported architecture: $(uname -m); exit 1 ;;
esac

# base
docker buildx build --load --progress=plain --target=base        --tag=gcc_alp:$TAG .
# vcpkg
docker buildx build --load --progress=plain --target=vcpkg       --tag=vcpkg_alp:$TAG .
# restinio-dev
docker buildx build --load --progress=plain --target=restiniodev --tag=restinio_dev:$TAG .
# build
docker buildx build --load --progress=plain --target=build       --tag=restinio_build:$TAG .
# restinio
docker buildx build --load --progress=plain --target=runtime     --tag=hello_restinio:$TAG .
