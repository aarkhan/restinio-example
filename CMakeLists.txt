cmake_minimum_required(VERSION 3.16)

project(hello_restinio
        DESCRIPTION "Example project using restinio"
        LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(APPLICATION ${PROJECT_NAME})

include(CMakePrintHelpers)

find_package(asio CONFIG REQUIRED)
find_package(restinio CONFIG REQUIRED)
find_package(unofficial-http-parser CONFIG REQUIRED)
find_package(fmt CONFIG REQUIRED)

add_executable(${APPLICATION})
target_sources(${APPLICATION} PRIVATE
    source/hello_world_minimal.cpp
)

target_link_libraries(${APPLICATION}
  restinio::restinio
  asio::asio
  unofficial::http_parser::http_parser
  fmt::fmt-header-only
)

if(UNIX)
  target_link_libraries(${APPLICATION}
   pthread
  )
endif()
